<?php

return [
    'image_mimes' => 'jpeg,jpg,png',
    'image_size'  => 5000,
];