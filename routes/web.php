<?php

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::group(['middleware' => ['web'], 'namespace' => 'Frontend'], function () {

    Route::post('posts/{post}/comments/', 'CommentController@store')->name('frontend.comments.store');
    Route::get('/', 'PostController@index');
    Route::get('/search', 'PostController@search')->name('frontend.search');
    Route::get('{alias}', 'PostController@onePost')->name('post')->where('alias', '[A-Za-z0-9-]+');
    Route::get('category/{category}', 'CategoryController@index')->name('frontend.categories');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['web'], 'prefix' => 'backend', 'namespace' => 'Backend'], function () {
    Route::resource('users', 'UserController');
});

Route::group(['middleware' => ['web'], 'prefix' => 'backend/roles', 'namespace' => 'Backend'], function () {
    Route::post('/', 'RoleController@store')->name('roles.store');
    Route::get('/', 'RoleController@index')->name('roles.index');
    Route::get('/{id}/edit', 'RoleController@edit');
    Route::get('/create', 'RoleController@create')->name('roles.create');
    Route::patch('/{role}', 'RoleController@update')->name('roles.update');
    Route::delete('/{role}', 'RoleController@destroy')->name('roles.destroy');
    Route::resource('roles', 'RoleController');
});

Route::group(['middleware' => ['web'], 'prefix' => 'backend/permissions', 'namespace' => 'Backend'], function () {
    Route::post('/', 'PermissionController@store')->name('permissions.store');
    Route::get('/', 'PermissionController@index')->name('permissions.index');
    Route::get('/{id}/edit', 'PermissionController@edit');
    Route::get('/create', 'PermissionController@create')->name('permissions.create');
    Route::delete('/{permission}', 'PermissionController@destroy')->name('permissions.destroy');
});

Route::group(['middleware' => ['web'], 'prefix' => 'backend/categories', 'namespace' => 'Backend'], function () {
    Route::post('/', 'CategoryController@store')->name('backend.categories.store');
    Route::get('/', 'CategoryController@index')->name('categories');
    Route::get('/create', 'CategoryController@create')->name('categories.create');
    Route::get('/{category}/edit', 'CategoryController@edit');
    Route::get('/{category}', 'CategoryController@show')->name('categories.show');
    Route::patch('/{category}', 'CategoryController@update')->name('categories.update');
    Route::delete('/{category}', 'CategoryController@destroy')->name('categories.destroy');
});

Route::group(['middleware' => ['web'], 'prefix' => 'backend/posts', 'namespace' => 'Backend'], function () {
    Route::post('/', 'PostController@store')->name('backend.posts.store');
    Route::get('/', 'PostController@index')->name('posts');
    Route::get('/create', 'PostController@create')->name('posts.create');
    Route::get('/{post}/edit', 'PostController@edit');
    Route::get('/{post}', 'PostController@show')->name('posts.show');
    Route::patch('/{post}', 'PostController@update')->name('posts.update');
    Route::delete('/{post}', 'PostController@destroy')->name('posts.destroy');
});

Route::group(['middleware' => ['web'], 'prefix' => 'backend/comments', 'namespace' => 'Backend'], function () {
    Route::post('/', 'CommentController@store')->name('comments.store');
    Route::get('/', 'CommentController@index')->name('comments');
    Route::get('/create', 'CommentController@create')->name('comments.create');
    Route::get('/{comment}/edit', 'CommentController@edit');
    Route::patch('/{comment}', 'CommentController@update')->name('comments.update');
    Route::delete('/{comment}', 'CommentController@destroy')->name('comments.destroy');

});
Route::group(['middleware' => ['web'], 'prefix' => 'backend/avaibility-post', 'namespace' => 'Backend'], function () {
    Route::post('/{id}', 'AvaibilityPostController@published')->name('completed-post.published');
    Route::delete('/{id}', 'AvaibilityPostController@unpublished')->name('completed-post.unpublished');
});

Route::group(['middleware' => ['web'], 'prefix' => 'backend/dashboard', 'namespace' => 'Backend'], function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');
});
//
//Route::get('/create_role_permission', function (){
//    $role = Role::create(['name' => 'Administer']);
//    $permission = Permission::create(['name' => 'Administer roles & permissions']);
//    auth()->user()->assignRole('Administer');
//    auth()->user()->givePermissionTo('Administer roles & permissions');
//});


