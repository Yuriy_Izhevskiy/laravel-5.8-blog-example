function deleteCategory(id)
{
    var id = id;
    var url = "categories/:id";
    url = url.replace(':id', id);
    $("#deleteForm").attr('action', url);
}

function formSubmit()
{
    $("#deleteForm").submit();
}


$(document).on("click", "i.del" , function() {
    $(this).parent().remove();
});


$(function() {
    $(document).on("change",".uploadFile", function()
    {
        var uploadFile = $(this);
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file

            reader.onloadend = function(){ // set image data as background of div
                //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
                if ($('#image').length) {
                    $('#image').remove();
                }
                uploadFile.closest(".imgUp").find('.imagePreview').css("background-image", "url("+this.result+")");
                uploadFile.closest(".imgUp").find('.imagePreviewFront').css("background-image", "url("+this.result+")");
            }
        }

    });
});