<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Comment;
use App\Models\Post;
use App\User;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
//        'user_id' => function() {return User::all()->random()->id;},
        'user_email' => $faker->unique()->safeEmail,
        'user_name' => $faker->name,
        'body'    => $faker->paragraph,
        'is_published' => rand(0, 1)
    ];
});
