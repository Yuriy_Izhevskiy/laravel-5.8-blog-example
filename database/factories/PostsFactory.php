<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Category;
use App\Models\Post;
use App\User;
use Faker\Generator as Faker;
use Cocur\Slugify\Slugify;

$factory->define(Post::class, function (Faker $faker) {
    $slugify = new Slugify();

    $title = $faker->sentence;
    return [
        'title' => $title,
        'description' => $faker->sentence,
        'alias' =>  $slugify->slugify($title),
        'body' => $faker->paragraph(30),
//        'user_id' => function() {return User::all()->random()->id;},
        'category_id' => function() {return Category::all()->random()->id;},
        'is_published' => rand(0, 1)
    ];
});
