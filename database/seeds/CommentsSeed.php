<?php

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Database\Seeder;

class CommentsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = Post::all();
        $arrayToSave = [];
//        factory(Comment::class, 20)->create();
        factory(Comment::class, 20)->make()->each(function (Comment $comment) use ($posts, &$arrayToSave) {
            $post = $posts->random();

            $comment->post_id = $post->getKey();

            $arrayToSave[] = $comment->toArray();
        });

        Comment::insert($arrayToSave);
    }
}
