<h2 class="text-center">Comments</h2>

<div class="card">
    <div class="card-body">
        <div class="row">
            @foreach($post->comment as $comment)
                <div class="col-md-2">
                    <div class="test">
                    <img   src="{{URL::asset($comment->imagePath)}}" alt="{{$comment->user_name}}" >
                    </div>
                    <p class="text-secondary text-center">{{$comment->created_at != null ? $comment->created_at->diffAsCarbonInterval() :  ''}}</p>
                </div>
                <div class="col-md-10">
                    <p>
                        <strong>{{$comment->user_name}}</strong>
                    </p>
                    <div class="clearfix"></div>
                    <p>{{$comment->body}}</p>
                </div>
            @endforeach


        </div>
    </div>
</div>