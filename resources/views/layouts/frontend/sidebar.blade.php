<!-- Sidebar Widgets Column -->
<div class="col-md-4">

    <!-- Search Widget -->
    <div class="card my-4">
        <h5 class="card-header">Search</h5>
        <div class="card-body">
            <div class="input-group">
                <form method="GET" action="{{route('frontend.search')}}" role="search">
                    <input type="text" class="form-control" name="q" placeholder="Search for...">
                    <button type="submit">GO</button>
                </form>
              </span>
            </div>
        </div>
    </div>

    <!-- Categories Widget -->
    @if ($categories->count())
        <div class="card my-4">
            <h5 class="card-header">Categories</h5>
            <div class="card-body">
                <div class="row">


                    <div class="col-lg-6">
                        <ul class="list-unstyled mb-0">
                            @foreach($categories as $category)
                                <li>
                                    <a href="{{route('frontend.categories', $category->name)}}">{{$category->name}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                </div>
            </div>
        </div>

    @endif
</div>