<h2 class="my-4">Create comment</h2>
<form method="POST" action="{{route('frontend.comments.store', $post->id)}}" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="title">Name:</label>
        <input type="text" class="input {{$errors->has('user_name') ? 'is-danger' : ''}}" name="user_name" placeholder="Tor" value="{{old('user_name')}}">
    </div>
    <div class="form-group">
        <label for="title">Email:</label>
        <input type="text" class="input {{$errors->has('user_email') ? 'is-danger' : ''}}" name="user_email" placeholder="example@em.com" value="{{old('user_email')}}">
    </div>

    <div class="form-group">
        <label for="body">Text</label>
        <textarea name="body" rows="10"  class="form-control input {{$errors->has('body') ? 'is-danger' : ''}}">{{old('body')}}</textarea>
    </div>

    <br>
    <div class="col-sm-2 imgUp">
        <div class="imagePreviewFront"></div>
        <label class="btn btn-primary">
            Upload<input type="file" name="image" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
        </label>
    </div>

    <button type="submit" class="btn btn-primary">Save</button>

</form>
