@extends('layouts.frontend.app')

@section('content')
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <!-- Blog Entries Column -->
            <div class="col-md-8">
                <div style="margin: 50px"> @include('success') @include('errors')</div>

                <h1 class="my-4">Page Heading<small>Secondary Text</small></h1>
                <div class="card-footer text-muted">
                    {{ $post->created_at->toDayDateTimeString() }}
                </div>

                <!-- Blog Post -->
                    <div class="card mb-4">
                        <div class="card" style="width:400px">
                        <img class="card-img-top"  src="{{URL::asset($post->imagePath)}}" alt="{{$post->title}}" height="200" width="200">
                        </div>
                        <div class="card-body">
                            <h2 class="card-title">{{ $post->title }}</h2>
                            <p class="card-text">
                                {{$post->body}}
                            </p>
                        </div>
                    </div>
            </div>
            @include('layouts.frontend.sidebar')
        </div>

        @if($post->comment->count())
            @include('layouts.frontend.blog-comments')
        @endif
        @include('frontend.form.create-comment')
    </div>
    <br>

@endsection