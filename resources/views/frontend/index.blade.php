@extends('layouts.frontend.app')

@section('content')
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
                <div style="margin: 50px"> @include('success') @include('errors')</div>

                <h1 class="my-4">Page Heading
                    <small>Secondary Text</small>
                </h1>

            @forelse ($posts as $post)
                <!-- Blog Post -->
                <div class="card mb-4">
                    <img class="card-img-top"  src="{{URL::asset($post->imagePath)}}" alt="{{$post->title}}" height="350" width="350">

                    <div class="card-body">
                        <h2 class="card-title">{{ $post->title }}</h2>
                        <p class="card-text">
                            {{mb_strimwidth($post->body, 0, 200, '...')}}
                        </p>
                        <a href="/{{$post->alias}}" class="btn btn-primary">Read More &rarr;</a>
                    </div>
                    <div class="card-footer text-muted">
                        {{ $post->created_at->toDayDateTimeString() }}
                    </div>
                </div>

                @empty
                    <div class="panel panel-default">
                        <div class="panel-heading">Not Found!!</div>

                        <div class="panel-body">
                            <p>Sorry! No post found.</p>
                        </div>
                    </div>
            @endforelse
                <!-- Pagination -->
                <div class="pagination justify-content-center mb-4">
                    {!! $posts->appends(['search' => request()->get('search')])->links() !!}
                </div>
            </div>
            @include('layouts.frontend.sidebar')

        </div>
        <!-- /.row -->

    </div>
@endsection
