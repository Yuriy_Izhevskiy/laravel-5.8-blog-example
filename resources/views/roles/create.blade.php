@extends('layouts.backend.app')

@section('title', '| Add Role')

@section('content')

    <div class='col-lg-4 col-lg-offset-4'>

        <h1><i class='fa fa-key'></i> Add Role</h1>
        <hr>

        <form action="{{route('roles.store')}} " method="POST">
            @csrf
            <div class="form-group">
                <label class="az-content-label">Name</label>
                <input type="text" class="input form-control {{$errors->has('name') ? 'is-danger' : ''}}" name="name"  value="{{old('name')}}">
            </div>

        <h5><b>Assign Permissions</b></h5>

        <div class='form-group'>

            @foreach ($permissions as $permission)
                <div class="form-check">
                    <label class="az-content-label">{{$permission->name}} {{ ucfirst($permission->name)}}</label>
                    <input class="form-check-input" type="checkbox" name="permissions[]" value="{{$permission->id}}">
                </div>
            @endforeach

        </div>

            <button type="submit" class="btn btn-primary">Add</button>

        </form>
        @include('errors')
    </div>

@endsection