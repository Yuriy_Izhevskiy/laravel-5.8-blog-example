@extends('layouts.backend.app')
@section('title', '| Edit Role')

@section('content')

    <div class='col-lg-4 col-lg-offset-4'>
        <h1><i class='fa fa-key'></i> Edit Role: {{$role->name}}</h1>
        <hr>

        <form action="{{route('roles.update', $role->id )}}" method="POST">

            @method('PATCH')
            @csrf

            <div class="form-group">
                <label class="az-content-label">Role Name</label>
                <input type="text" class="input form-control {{$errors->has('name') ? 'is-danger' : ''}}" name="name"  value="{{$role->name}}">
            </div>

        <h5><b>Assign Permissions</b></h5>
            @foreach ($permissions as $permission)
                <div class="form-check">
                    <label class="az-content-label">{{$permission->name}}</label>
                    <input class="form-check-input" type="checkbox" name="permissions[]" value="{{$permission->id}}" {{$role->hasPermissionTo($permission->id) ? 'checked' : ''}}>
                </div>
            @endforeach
        <br>
            <button type="submit" class="btn btn-primary">Edit</button>

        </form>
    </div>
    @include('errors')
@endsection