@extends('layouts.backend.app')

@section('title', '| Add User')

@section('content')

    <div class='col-lg-4 col-lg-offset-4'>

        <h1><i class='fa fa-user-plus'></i> Add User</h1>
        <hr>

        <form action="{{route('users.store')}}" method="POST">
            @csrf
{{--        {{ Form::open(array('url' => 'users')) }}--}}

        <div class="form-group">
            <input type="text" class="input form-control {{$errors->has('name') ? 'is-danger' : ''}}" name="name"  value="{{old('name')}}">
{{--            {{ Form::label('name', 'Name') }}--}}
{{--            {{ Form::text('name', '', array('class' => 'form-control')) }}--}}
        </div>

        <div class="form-group">
            <input type="text" class="input form-control {{$errors->has('email') ? 'is-danger' : ''}}" name="email"  value="{{old('email')}}">
{{--            {{ Form::label('email', 'Email') }}--}}
{{--            {{ Form::email('email', '', array('class' => 'form-control')) }}--}}
        </div>

        <div class='form-group'>
            <div class="form-group">
                @foreach ($roles as $role)
                <div class="form-check">
                    <label class="az-content-label">{{$role->name}}</label>
                    <input class="form-check-input" type="checkbox" name="roles[]" value="{{$role->id}}">
                </div>
                @endforeach
            </div>


{{--            @foreach ($roles as $role)--}}
{{--                {{ Form::checkbox('roles[]',  $role->id ) }}--}}
{{--                {{ Form::label($role->name, ucfirst($role->name)) }}<br>--}}

{{--            @endforeach--}}
        </div>

        <div class="form-group">
            <label class="az-content-label">Password</label>
            <input type="text" class="input form-control {{$errors->has('password') ? 'is-danger' : ''}}" name="password"  value="{{old('password')}}">

{{--            {{ Form::label('password', 'Password') }}<br>--}}
{{--            {{ Form::password('password', array('class' => 'form-control')) }}--}}

        </div>

        <div class="form-group">
            <label class="az-content-label">Confirm Password'</label>
            <input type="text" class="input form-control {{$errors->has('password_confirmation') ? 'is-danger' : ''}}" name="password_confirmation"  value="{{old('password_confirmation')}}">
{{--            {{ Form::label('password', 'Confirm Password') }}<br>--}}
{{--            {{ Form::password('password_confirmation', array('class' => 'form-control')) }}--}}

        </div>

            <button type="submit" class="btn btn-primary">Add</button>
{{--        {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}--}}

{{--        {{ Form::close() }}--}}
        </form>
    </div>

@endsection