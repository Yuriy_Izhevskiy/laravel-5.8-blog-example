@extends('layouts.backend.app')

@section('title', '| Edit User')

@section('content')

    <div class='col-lg-4 col-lg-offset-4'>

        <h1><i class='fa fa-user-plus'></i> Edit {{$user->name}}</h1>
        <hr>
        <form action="{{route('users.update', $user->id )}}" method="POST">
            @method('PATCH')
            @csrf

        <div class="form-group">

            <div class="form-group">
                <label class="az-content-label">Name</label>
                <input type="text" class="input form-control {{$errors->has('name') ? 'is-danger' : ''}}" name="name"  value="{{$user->name}}">
            </div>
        </div>

        <div class="form-group">

            <div class="form-group">
                <label class="az-content-label">Email</label>
                <input type="text" class="input form-control {{$errors->has('email') ? 'is-danger' : ''}}" name="email"  value="{{$user->email}}">
            </div>
        </div>

        <h5><b>Give Role</b></h5>

        <div class='form-group'>

                @foreach ($roles as $role)
                    <div class="form-check">
                        <label class="az-content-label">{{$role->name}} </label>
                        <input class="form-check-input" type="checkbox" name="roles[]" value="{{$role->id}}" {{$checked ? 'checked' : '' }}>
                    </div>
                @endforeach

        </div>

            <div class="form-group">
                <label class="az-content-label">Password</label>
                <input type="text" class="input form-control {{$errors->has('password') ? 'is-danger' : ''}}" name="password"  value="{{$user->password}}">
            </div>



            <div class="form-group">
                <label class="az-content-label">Confirm Password'</label>
                <input type="text" class="input form-control {{$errors->has('password_confirmation') ? 'is-danger' : ''}}" name="password_confirmation"  value="{{$user->password_confirmation}}">
            </div>
            <button type="submit" class="btn btn-primary">Add</button>

        </form>

    </div>

@endsection