@extends('layouts.backend.app')

@section('content')
    <div class="card-body">
        <div class="row row-sm">
            <div class="col-6 col-lg-3">
                <label class="az-content-label">Post</label>
                <div class="panel-body">
                    <table class="table table-condensed table-striped table-bordered table-hover">
                        <tbody><tr>
                            <th width="150px">Всего</th>
                            <td>{{$posts}}</td>
                        </tr>
                        <tr>
                            <th>Опубликованых</th>
                            <td>{{$postsActive}}</td>
                        </tr>
                        <tr>
                            <th>Не опубликованых</th>
                            <td>{{$postsDisabled}}</td>
                        </tr>
                        </tbody></table>
                </div>

            </div><!-- col -->
            <div class="col-6 col-lg-3">
                <label class="az-content-label">Category</label>
                <div class="panel-body">
                    <table class="table table-condensed table-striped table-bordered table-hover">
                        <tbody><tr>
                            <th width="150px">Всего</th>
                            <td>{{$categories}}</td>
                        </tr>
                        </tbody></table>
                </div>
            </div><!-- col -->
            <div class="col-6 col-lg-3 mg-t-20 mg-lg-t-0">
                <label class="az-content-label">Comment</label>
                <div class="panel-body">
                    <table class="table table-condensed table-striped table-bordered table-hover">
                        <tbody><tr>
                            <th width="150px">Всего</th>
                            <td>{{$comments}}</td>
                        </tr>
                        <tr>
                            <th>Опубликованых</th>
                            <td>{{$commentsActive}}</td>
                        </tr>
                        <tr>
                            <th>Не опубликованых</th>
                            <td>{{$commentsDisabled}}</td>
                        </tr>
                        </tbody></table>
                </div>
            </div><!-- col -->

        </div><!-- row -->
    </div>

@endsection