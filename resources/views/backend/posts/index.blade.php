@extends('layouts.backend.app')

@section('content')
    @include('success')
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Posts</h2>
                    </div>
                    <div class="col-sm-6">
                        <a href="posts/create" class="btn btn-success"><i class="material-icons">&#xE147;</i><span>Create New Post</span></a>
                    </div>
                </div>
            </div>

            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Date Created</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                @forelse($posts as $post)
                    <tr>
                        <td>{{$post->id}}</td>
                        <td><a href="posts/{{$post->id}}">
                                <img class="avatar" src="{{URL::asset($post->imagePath)}}" alt="{{$post->title}}" height="70" width="70"> {{$post->title}}
                            </a>
                        </td>
                        <td>{{$post->created_at}}</td>
                        <td>
                            <form action="avaibility-post/{{$post->id}}" method="POST">
                                @csrf

                                @if ($post->is_published)
                                    @method('DELETE')
                                @endif
                                <span class="status ">
                                    <input class="form-check-input" type="checkbox" name="is_published" {{$post->is_published ? 'checked' : ''}} onChange="this.form.submit()">
                                </span>
                            </form>
                        </td>
                        <td>
                            <form method="POST" action="posts/{{$post->id}}">
                                @method('DELETE')
                                @csrf
                                <a href="posts/{{$post->id}}" class="view" title="" data-toggle="tooltip" data-original-title="View"><i class="material-icons"></i></a>
                                <a href="posts/{{$post->id}}/edit" class="edit"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                <button type="submit" class="delete" onclick="return confirm('Are you sure you want to delete this Post?');">Delete</button>
                            </form>
                        </td>
                    </tr>

                    @empty
                        <tr>
                            <td colspan="5">No category available.</td>
                        </tr>
                    @endforelse

                </tbody>
            </table>
            <div class="clearfix">
                {!! $posts->links() !!}
            </div>
        </div>


@endsection