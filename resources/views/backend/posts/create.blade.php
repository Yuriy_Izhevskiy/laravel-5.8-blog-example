@extends('layouts.backend.app')

@section('content')
    <div class="table-wrapper">
        <div class="card uper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h4> Add<b> new post</b></h4>
                    </div>

                </div>
            </div>
            <div class="card-body">

                <form method="POST" action="{{route('backend.posts.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="title">Title:</label>
                        <input type="text" class="input {{$errors->has('title') ? 'is-danger' : ''}}" name="title" placeholder="Sint et esse dolorum fuga repudiandae et atque." value="{{old('title')}}">
                    </div>

                    <div class="form-group">
                        <label for="alias">Alias:</label>
                        <input type="text" class="input {{$errors->has('alias') ? 'is-danger' : ''}}" name="alias" placeholder="sint-et-esse-dolorum-fuga-repudiandae-et-atque" value="{{old('alias')}}">
                    </div>


                    <div class="form-group">
                        <label for="description">Description:</label>
                        <input type="text" class="input {{$errors->has('description') ? 'is-danger' : ''}}" name="description" placeholder="Magni sapiente qui ullam voluptatum rerum mollitia." value="{{old('description')}}">
                    </div>

                    <div class="form-group">
                        <label for="body">Text</label>
                        <textarea name="body" rows="10"  class="form-control {{$errors->has('body') ? 'is-danger' : ''}}">{{old('body')}}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="category_id">Category</label>
                        <select  class="form-control" name="category_id">
                            <option  value="">Selected category</option>
                            @foreach($categories as $category)
                                @if (old('category_id') == $category->id)
                                    <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                                @else
                                    <option  value="{{$category->id}}">{{$category->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <div class="form-check">

                            <input class="form-check-input" type="checkbox" name="is_published" {{ old('is_published') ? 'checked' : '' }}>
                            <label class="form-check-label" for="defaultCheck1">Post entry</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-2 imgUp">
                            <div class="imagePreview"></div>
                            <label class="btn btn-primary">
                                Upload<input type="file" name="image" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
                            </label>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
            @include('errors')
        </div>
    </div>
@endsection