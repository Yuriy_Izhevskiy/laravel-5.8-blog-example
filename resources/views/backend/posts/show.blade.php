
@extends('layouts.backend.app')

@section('content')
    <div class="table-wrapper">
        <div class="card uper">

            <div class="table-title">
                <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
                <div class="row">
                    <div class="col-sm-6">
                        <h4> Show<b> post</b></h4>
                    </div>
                    <div class="col-sm-6">

                        <form action="{{$post->id}}" method="POST">
                            @method('DELETE')
                            @csrf

                            @can('Edit Post')
                                <a href="{{$post->id}}/edit" class="btn btn-success"><i class="material-icons"></i> <span>Edit</span></a>
                            @endcan

                            @can('Delete Post')

                                <button type="submit" onclick="return confirm('Are you sure you want to delete this Post?');" class="btn btn-danger"><i class="material-icons"></i> <span>Delete</span></button>
                            @endcan
                        </form>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="form-group">
                    <p class="product-title">{{$post->title}}</p>
                    <div class="text-success">
                        <small class="text-muted">Дата создания</small> {{$post->created_at}}

                    </div>
                    <div class="text-success">
                        <small class="text-muted">Дата последнего обновления</small> {{$post->updated_at}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection