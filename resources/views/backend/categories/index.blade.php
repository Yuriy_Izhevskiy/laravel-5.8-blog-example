@extends('layouts.backend.app')

@section('content')
    @include('success')
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h4> Category<b> list</b></h4>
                    </div>
                    <div class="col-sm-6">
                        <a href="categories/create" class="btn btn-success"><i class="material-icons">&#xE147;</i><span>Create New Category</span></a>
                    </div>
                </div>
            </div>

            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($categories as $category)
                        <tr>
                            <td>{{$category->id}}</td>
                            <td>{{$category->name}}</td>
                            <td>
                                <form method="POST" action="categories/{{$category->id}}">
                                    @method('DELETE')
                                    @csrf
                                    <div class="field">
                                        <div class="control">
                                            <a href="categories/{{$category->id}}" class="view" title="" data-toggle="tooltip" data-original-title="View"><i class="material-icons"></i></a>
                                            <a href="categories/{{$category->id}}/edit" class="edit"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                            <button type="submit" class="delete" onclick="return confirm('Are you sure you want to delete this Category?');">Delete</button>
                                        </div>
                                    </div>
                                </form>
                            </td>
                        </tr>

                    @empty
                        <tr>
                            <td colspan="5">No category available.</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
            <div class="clearfix">
                {!! $categories->links() !!}
            </div>
        </div>

    @include('backend.categories.modal')

    @endsection