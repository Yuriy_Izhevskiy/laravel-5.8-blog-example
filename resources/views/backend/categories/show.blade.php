@extends('layouts.backend.app')

@section('content')
    <div class="table-wrapper">
        <div class="card uper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h4> Show<b> category</b></h4>
                    </div>

                </div>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <p class="product-title">{{$category->name}}</p>
                    <div class="text-success">
                        <small class="text-muted">Дата создания</small> {{$category->created_at}}

                    </div>
                    <div class="text-success">
                        <small class="text-muted">Дата последнего обновления</small> {{$category->updated_at}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection