@extends('layouts.backend.app')

@section('content')
    <div class="table-wrapper">
        <div class="card uper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h4> Add<b> new category</b></h4>
                    </div>

                </div>
            </div>
            <div class="card-body">

                <form method="POST" action="{{route('backend.categories.store')}}">
                    <div class="form-group">
                        @csrf
                        <label for="name">Name:</label>
                        <input type="text" class="input {{$errors->has('name') ? 'is-danger' : ''}}" name="name" placeholder="Title" value="{{old('name')}}">
                    </div>

                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
            @include('errors')
        </div>
    </div>
@endsection