@extends('layouts.backend.app')

@section('content')
    <div class="table-wrapper">
        <div class="card uper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h4> Edit<b> category</b></h4>
                    </div>

                </div>
            </div>
            <div class="card-body">

                <form method="POST" action="{{route('categories.update', $category->id)}}">
                    <div class="form-group">
                        @csrf
                        @method('PATCH')
                        <label for="name">Name:</label>
                        <input type="text" class="input {{$errors->has('name') ? 'is-danger' : ''}}" name="name" placeholder="Title" value="{{$category->name}}">
                    </div>

                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
            @include('errors')
        </div>
    </div>
@endsection