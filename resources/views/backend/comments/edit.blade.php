@extends('layouts.backend.app')

@section('content')
    <form method="POST" action="{{route('comments.update', $comment->id)}}" enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Create comment</h2>
                    </div>

                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>

                </div>
            </div>
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">Main</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">Info</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">Image</a>
                </li>
            </ul><!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="tabs-1" role="tabpanel">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="base">
                            <table class="table table-bordered table-condensed">
                                <tbody><tr>
                                    <th class="vertical-align" width="160px">Visibility:</th>
                                    <td>
                                        <div class="form-group">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="is_published" {{$comment->is_published ? 'checked' : ''}}>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="vertical-align">ID Post:</th>
                                    <td>
                                        <input type="number" name="post_id" value="{{$comment->post_id}}" class="input {{$errors->has('post_id') ? 'is-danger' : ''}}">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tabs-2" role="tabpanel">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="base">
                            <table class="table table-bordered table-condensed">
                                <tbody>
                                <tr>
                                    <th class="vertical-align">User name:</th>
                                    <td>
                                        <input type="text" class="input {{$errors->has('user_name') ? 'is-danger' : ''}}" name="user_name"  value="{{$comment->user_name}}">
                                    </td>
                                </tr>
                                <tr>
                                    <th class="vertical-align">User Email:</th>
                                    <td>
                                        <input type="text" name="user_email" value="{{$comment->user_email}}" class="input {{$errors->has('user_email') ? 'is-danger' : ''}}">
                                    </td>
                                </tr>
                                <tr>
                                    <th class="vertical-align" width="160px">Comment:</th>
                                    <td>
                                        <div class="form-group">
                                            <textarea name="body" rows="10"  class="input {{$errors->has('body') ? 'is-danger' : ''}}">{{$comment->body}}</textarea>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tabs-3" role="tabpanel">
                    <br>
                    <div class="col-sm-2 imgUp">
                        <div class="imagePreview">
                            <img id="image" src="{{URL::asset($comment->imagePath)}}" alt="{{$comment->user_name}}"  style="width: 350px; height: 350px; background-size: cover; background-repeat: no-repeat;display: inline-block;    box-shadow: 0px -3px 6px 2px rgba(0,0,0,0.2);">
                        </div>
                        <label class="btn btn-primary">
                            Upload<input type="file" name="image" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
                        </label>
                    </div>

                </div>

            </div>
        </div>
    @include('errors')
    </div>
    </form>
@endsection