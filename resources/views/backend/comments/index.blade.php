@extends('layouts.backend.app')

@section('content')
    @include('success')
    <div class="table-wrapper">
        <div class="table-title">
            <div class="row">
                <div class="col-sm-6">
                    <h2>Comments</h2>
                </div>
                <div class="col-sm-6">
                    <a href="comments/create" class="btn btn-success"><i class="material-icons">&#xE147;</i><span>Create New Comment</span></a>
                </div>
            </div>
        </div>

        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>User</th>
                <th>ID Post</th>
                <th>Comment</th>
                <th>Visibility</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>

            @forelse ($comments as $comment)
                <tr>
                    <td>{{$comment->id}}</td>
                    <td>
                        <img class="avatar" src="{{URL::asset($comment->imagePath)}}" alt="{{$comment->user_name}}" height="70" width="70"> {{$comment->user_name}}
                    </td>
                    <td>
                        <a href="posts/{{ $comment->post['id'] }}/edit">  {{ $comment->post['id'] }}</a>
                    </td>
                    <td> {{mb_strimwidth($comment->body, 0, 80, '...')}}</td>
                    <td><span class="status text-{{$comment->published}}">&bull;</span></td>
                    <td>
                        <form method="POST" action="comments/{{$comment->id}}">
                            @method('DELETE')
                            @csrf
                            <a href="comments/{{$comment->id}}/edit" class="edit"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                            <button type="submit" class="delete" onclick="return confirm('Are you sure you want to delete this Post?');">Delete</button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="2">No comment available.</td>
                </tr>
            @endforelse

            </tbody>
        </table>
        <div class="clearfix">
            {!! $comments->links() !!}
        </div>
    </div>


@endsection