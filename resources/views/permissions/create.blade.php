@extends('layouts.backend.app')

@section('title', '| Create Permission')

@section('content')

    <div class='col-lg-4 col-lg-offset-4'>

        <h1><i class='fa fa-key'></i> Add Permission</h1>
        <br>
        <form action="{{route('permissions.store')}}" method="POST">
            @csrf
            <div class="form-group">
                <label class="az-content-label">Name</label>
                <input type="text" class="input form-control {{$errors->has('name') ? 'is-danger' : ''}}" name="name"  value="{{old('name')}}">
            </div>

            <br>
        @if(!$roles->isEmpty()) //If no roles exist yet
        <h4>Assign Permission to Roles</h4>
            @foreach ($roles as $role)
                <div class="form-check">
                    <label class="az-content-label">{{$role->name}}</label>
                    <input class="form-check-input" type="checkbox" name="roles[]" value="{{$role->id}}">
                </div>
            @endforeach
        @endif
        <br>
            <button type="submit" class="btn btn-primary">Add</button>
        </form>

    </div>
    @include('errors')
@endsection