@extends('layouts.backend.app')
@section('title', '| Edit Permission')

@section('content')

    <div class='col-lg-4 col-lg-offset-4'>

        <h1><i class='fa fa-key'></i> Edit {{$permission->name}}</h1>
        <br>

        <form action="{{route('permissions.update', $permission->id )}}" method="POST">

            @method('PATCH')
            @csrf

        <div class="form-group">
            <label class="az-content-label">Permission Name</label>
            <input type="text" class="input form-control {{$errors->has('name') ? 'is-danger' : ''}}" name="name"  value="{{$permission->name}}">
        </div>



        <br>
            <button type="submit" class="btn btn-primary">Edit</button>

        </form>

    </div>
    @include('errors')
@endsection