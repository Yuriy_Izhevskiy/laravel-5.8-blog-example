<?php

namespace App\Http\Requests\Frontend\Comments;

use Illuminate\Foundation\Http\FormRequest;

class FieldRequestCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_name' => 'required|min:3|max:50',
            'user_email' => 'required|email|unique:comments',
            'body' => 'required|max:1500|min:3',
            'image' => 'nullable|mimes:' . config('app.image_mimes') . '|max:' . config('app.image_size'),
        ];
    }
}
