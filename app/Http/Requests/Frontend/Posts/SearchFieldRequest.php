<?php

namespace App\Http\Requests\Frontend\Posts;

use Illuminate\Foundation\Http\FormRequest;

class SearchFieldRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'q' => 'required|min:3|max:256',
            ];
    }

    public function messages()
    {
        return [
            'q.required' => "You must enter the desired sentence or word.",
            ];
    }
}