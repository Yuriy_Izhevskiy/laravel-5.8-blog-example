<?php

namespace App\Http\Requests\Backend\Comments;

use App\Models\Post;
use Illuminate\Foundation\Http\FormRequest;

class FieldRequestCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_name' => 'required|min:3|max:50',
            'user_email' => 'required|email',
            'body' => 'required|max:1500|min:3',
            'image' => 'nullable|mimes:' . config('app.image_mimes') . '|max:' . config('app.image_size'),
            'is_published' => 'in:on',
            'post_id' => 'required|integer|exists:posts,id',
        ];
    }
}
