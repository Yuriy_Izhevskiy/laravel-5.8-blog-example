<?php

namespace App\Http\Requests\Backend\Posts;

use App\Models\Category;
use Illuminate\Foundation\Http\FormRequest;

class FieldRequestCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255|min:3',
            'body' => 'max:1500|min:3',
            'description' => 'max:300|min:3',
            'alias' => 'max:2000',
            'category_id' => 'required|integer|max:'.Category::latest('id')->first()->id .'|min:1',
            'is_published' => 'in:on',
            'image' => 'nullable|mimes:' . config('app.image_mimes') . '|max:' . config('app.image_size')
        ];
    }
}
