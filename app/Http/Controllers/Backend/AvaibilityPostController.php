<?php

namespace App\Http\Controllers\Backend;

use App\Models\Post;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AvaibilityPostController extends Controller
{
    public function published(Post $post)
    {
        $post->complete($post->id);
        return back()->with('success', "Post $post->id is published successfully");
    }

    public function unpublished(Post $post)
    {
        $post->incomplete($post->id);
        return back()->with('success', "Post $post->id is successfully unpublished");
    }
}
