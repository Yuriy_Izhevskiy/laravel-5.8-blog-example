<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\ImageHelper;
use App\Http\Requests\Backend\Comments\FieldRequestCreate;
use App\Http\Requests\Backend\Comments\FieldRequestUpdate;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CommentController extends Controller
{
    protected $prefix = 'backend.comments';
    protected $prefixRedirect = 'backend/comments';


    public function index()
    {
        $comments = Comment::paginate(10);

        return view($this->prefix.'.index', ['comments' => $comments]);
    }


    public function create()
    {
        return view($this->prefix.'.create');
    }


    public function store(FieldRequestCreate $request, Comment $comment)
    {
        $comment->body = $request->body;
        $comment->is_published = (boolean) $request->is_published;
        $comment->post_id = $request->post_id;
        $comment->user_name = $request->user_name;
        $comment->user_email = $request->user_email;

        $imageName = null;
        if ($request->has('image')) {
            $imageName = $this->galleryUpload($request)['file_name'];
        }
        $comment->image = $imageName;
        $comment->save();
        return redirect($this->prefixRedirect)->with('success', 'Comment is successfully create');

    }

    public function galleryUpload($request)
    {
        $name = ImageHelper::generateName(Comment::GALLERY_PATH, $request->file('image'));
        $image = Storage::disk('public')->putFileAs(Comment::GALLERY_PATH, $request->file('image'), $name);
        return [
            'file'      => Storage::url($image),
            'file_name' => $name,
        ];
    }
    public function edit(Comment $comment)
    {
        return view($this->prefix.'.edit', ['comment' => $comment]);
    }

    public function update(FieldRequestUpdate $request, Comment $comment)
    {
        $requestData = $request->all();
        $requestData['is_published'] = $request->has('is_published') ? true : false;

        if ($request->has('image')) {
            $requestData['image'] = $this->galleryUpload($request)['file_name'];
        }

        $comment->update($requestData);

        return redirect($this->prefixRedirect)->with('success',  'Id ' .$comment->id . ' is successfully updated');
    }


    public function destroy(Comment $comment)
    {
        $comment->delete();
        return redirect($this->prefixRedirect)->with('success', 'Comment №"'. $comment->id . '" is successfully deleted');
    }
}
