<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\Categories\FieldRequestCreate;
use App\Http\Requests\Backend\Categories\FieldRequestUpdate;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    protected $prefix = 'backend.categories';
    protected $prefixRedirect = 'backend/categories';

    public function index()
    {
        $categories = Category::paginate(10);
        return view($this->prefix.'.index', ['categories' => $categories]);
    }


    public function create()
    {
       return view($this->prefix.'.create');
    }


    public function store(FieldRequestCreate $request)
    {
        Category::create($request->validated());
        return redirect($this->prefixRedirect)->with('success', 'Category "'. $request->name . '" is successfully create');
    }


    public function show(Category $category)
    {
        return view($this->prefix.'.show', ['category' => $category]);
    }


    public function edit(Category $category)
    {
        return view($this->prefix.'.edit', ['category' => $category]);
    }


    public function update(FieldRequestUpdate $request, Category $category)
    {
        $category->update($request->all());
        return redirect($this->prefixRedirect)->with('success', 'Category "'. $request->name . '" is successfully updated');
    }


    public function destroy(Category $category)
    {
        $category->delete();

        return redirect($this->prefixRedirect)->with('success', 'Category "'. $category->name . '" is successfully deleted');
    }
}
