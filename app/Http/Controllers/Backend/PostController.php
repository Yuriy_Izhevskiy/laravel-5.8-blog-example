<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\ImageHelper;
use App\Http\Requests\Backend\Posts\FieldRequestCreate;
use App\Http\Requests\Backend\Posts\FieldRequestUpdate;
use App\Http\Requests\Posts\UploadImageRequest;
use App\Models\Category;
use App\Models\Post;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\UploadTrait;
use Illuminate\Support\Facades\Storage;
use Cocur\Slugify\Slugify;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    use UploadTrait;

    protected $prefix = 'backend.posts';
    protected $prefixRedirect = 'backend/posts';

    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    public function index()
    {
        $posts = Post::paginate(10);
        return view($this->prefix.'.index', ['posts' => $posts]);
    }


    public function create()
    {
        $categories = Category::select('id', 'name')->get();

        return view($this->prefix.'.create', ['categories' => $categories]);
    }


    public function store(FieldRequestCreate $request, Post $post)
    {
        $post  = new Post($request->all());
//        $post->fill($request->all());
        $post->alias = $this->createAlias($request);
        $imageName = null;
        if ($request->has('image')) {
          $imageName = $this->galleryUpload($request)['file_name'];
        }
        $post->image = $imageName;

        $post->save();
        return redirect($this->prefixRedirect)->with('success', 'Post is successfully create');
    }

    public function createAlias($request)
    {
        $slugify = new Slugify();

        if (is_null($request->alias)) {
            return $slugify->slugify($request->title);
        }

        return $request->alias;
    }

    public function galleryUpload($request)
    {
        $name = ImageHelper::generateName(Post::GALLERY_PATH, $request->file('image'));
        $image = Storage::disk('public')->putFileAs(Post::GALLERY_PATH, $request->file('image'), $name);
        return [
            'file'      => Storage::url($image),
            'file_name' => $name,
        ];
    }


    public function show(Post $post)
    {
        $categories = Category::select('id', 'name')->get();

        return view($this->prefix.'.'.__FUNCTION__, ['post' => $post, 'categories' => $categories]);
    }


    public function edit(Post $post)
    {

        $categories = Category::select('id', 'name')->get();

        return view($this->prefix.'.edit', ['post' => $post, 'categories' => $categories]);
    }


    public function update(FieldRequestUpdate $request, Post $post)
    {

        $this->authorize('update', $post);

        $requestData = $request->all();
        $requestData['is_published'] = $request->has('is_published');

        if ($request->has('image')) {
            $requestData['image'] = $this->galleryUpload($request)['file_name'];
        }

        $post->update($requestData);

        return redirect($this->prefixRedirect)->with('success',  $request->title . ' is successfully updated');
    }


    public function destroy(Post $post)
    {
        $post->delete();
        return redirect($this->prefixRedirect)->with('success', 'Post №"'. $post->id . '" is successfully deleted');
    }
}
