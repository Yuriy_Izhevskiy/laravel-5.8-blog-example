<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Roles\FieldRequestCreate;
use App\Http\Requests\Roles\FieldRequestUpdate;
use Illuminate\Http\Request;

use Auth;
use App\Http\Controllers\Controller;
//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Session;

class RoleController extends Controller {

    protected $prefixRedirect = 'backend/roles';

    public function __construct() {
        $this->middleware(['auth', 'isAdmin']);//isAdmin middleware lets only users with a //specific permission permission to access these resources
    }


    public function index() {
        $roles = Role::all();//Get all roles

        return view('roles.index')->with('roles', $roles);
    }


    public function create() {
        $permissions = Permission::all();//Get all permissions

        return view('roles.create', ['permissions'=>$permissions]);
    }

    public function store(FieldRequestCreate $request) {
        //Validate name and permissions field
        $name = $request->name;
        $role = new Role();
        $role->name = $name;

        $permissions = $request['permissions'];

        $role->save();
        //Looping thru selected permissions
        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail();
            //Fetch the newly created role and assign permission
            $role = Role::where('name', '=', $name)->first();
            $role->givePermissionTo($p);
        }

        return redirect($this->prefixRedirect)->with('success', 'Role '. $role->name.' is successfully create');

    }

    public function show($id) {
        return redirect('roles');
    }

    public function edit($id) {
        $role = Role::findOrFail($id);
        $permissions = Permission::all();

        return view('roles.edit', compact('role', 'permissions'));
    }

    public function update(FieldRequestUpdate $request, Role $role)
    {
        $role = Role::findOrFail($role->id);//Get role with the given id
        //Validate name and permission fields

        $input = $request->except(['permissions']);
        $permissions = $request['permissions'];
        $role->fill($input)->save();

        $p_all = Permission::all();//Get all permissions

        foreach ($p_all as $p) {
            $role->revokePermissionTo($p); //Remove all permissions associated with role
        }

        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail(); //Get corresponding form //permission in db
            $role->givePermissionTo($p);  //Assign permission to role
        }

        return redirect($this->prefixRedirect)->with('success', 'Role'. $role->name.' updated!');}


    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();

        return redirect($this->prefixRedirect)->with('success', 'Role deleted!');
    }
}