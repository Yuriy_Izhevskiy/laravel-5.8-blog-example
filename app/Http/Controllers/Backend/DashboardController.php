<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use App\Models\Comment;

class DashboardController extends Controller
{

    protected $prefix = 'backend.dashboard';
    protected $prefixRedirect = 'backend/dashboard';

    public function index()
    {

        $comments = Comment::count();
        $commentsDisabled  = Comment::whereIsPublished(false)->count();
        $commentsActive   = Comment::whereIsPublished(true)->count();

        $posts = Post::count();
        $postsDisabled = Post::whereIsPublished(false)->count();
        $postsActive = Post::whereIsPublished(true)->count();

        $categories = Category::count();

        return view($this->prefix.'.index', [
            'posts' => $posts,
            'postsDisabled'=> $postsDisabled,
            'postsActive' => $postsActive,
            'comments' => $comments,
            'commentsDisabled' => $commentsDisabled,
            'commentsActive' => $commentsActive,
            'categories' => $categories
        ]);
    }
}