<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\Frontend\Posts\SearchFieldRequest;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    protected $prefix = 'frontend';
    protected $prefixRedirect = 'frontend';

    public $categories;

    public function __construct()
    {
        $this->categories = Category::all();
    }

    public function index()
    {
        $posts = Post::onlyActive()
            ->simplePaginate(5);

        return view($this->prefix.'.index', ['posts' => $posts, 'categories' => $this->categories]);
    }

    public function search(SearchFieldRequest $request)
    {
        $key = trim($request->get('q'));

        $key = "%$key%";
        $posts = Post::onlyActive()
            ->where('title', 'like', $key)
            ->orWhere('description', 'like', $key)
            ->orWhere('body', 'like', $key)
            ->paginate(5);

        return view($this->prefix.'.index', ['posts' => $posts, 'categories' => $this->categories]);
    }

    public function onePost(Request $request)
    {
        $post = Post::onlyActive()
            ->whereAlias($request->alias)
            ->first();

        $categories = Category::all();

        return view($this->prefix.'.onePost', ['post' => $post, 'categories' => $categories]);
    }
}
