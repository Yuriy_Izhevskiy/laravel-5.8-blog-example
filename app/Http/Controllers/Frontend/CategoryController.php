<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Models\Post;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    protected $prefix = 'frontend';
    protected $prefixRedirect = 'frontend';

    public function index($name)
    {
        $category = Category::where('name', $name)->first();

        $posts =  $category->post()
            ->onlyActive()
            ->select(Post::selectArrayWithOutContent)
            ->with(['category'])
            ->orderBy('created_at', 'desc')
            ->paginate(2);


        return view($this->prefix.'.index', ['posts' => $posts, 'categories' => Category::all()]);
    }

    public function getAll()
    {
        $categories =  Category::withCount('post')->get();
        return $categories;
    }
}
