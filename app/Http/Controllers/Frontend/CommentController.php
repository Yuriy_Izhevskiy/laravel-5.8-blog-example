<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\ImageHelper;
use App\Http\Requests\Frontend\Comments\FieldRequestCreate;
use App\Models\Comment;
use App\Models\Post;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CommentController extends Controller
{
    public function store(FieldRequestCreate $request, Post $post)
    {
        $imageName = null;
        if ($request->has('image')) {
            $imageName = $this->galleryUpload($request)['file_name'];
        }

        $input = $request->all();
        $input['image'] = $imageName;
        $post->comment()->create($input);

        return back()->with('success', 'The comment was successfully sent for moderation.');
    }

    public function galleryUpload($request)
    {
        $name = ImageHelper::generateName(Comment::GALLERY_PATH, $request->file('image'));

        $image = Storage::disk('public')->putFileAs(Comment::GALLERY_PATH, $request->file('image'), $name);
        return [
            'file'      => Storage::url($image),
            'file_name' => $name,
        ];
    }
}
