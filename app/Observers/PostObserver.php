<?php

namespace App\Observers;

use App\Http\Requests\Backend\Posts\FieldRequestUpdate;
use App\Models\Post;
use http\Env\Request;
use Illuminate\Support\Facades\Storage;

class PostObserver
{
    /**
     * Handle the post "created" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function created(Post $post)
    {
        //
    }

    /**
     * Handle the post "updated" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function updated(Post $post)
    {
        //
    }

    public function updating(Post $post)
    {
        /*** Delete old image before update image*/
        $oldNameImage = $post->getOriginal('image');

        $fileExists = Storage::disk('public')->exists(Post::GALLERY_PATH . $oldNameImage);

        if($post->isDirty('image') && $fileExists){
            Storage::disk('public')->delete(Post::GALLERY_PATH . $oldNameImage);
        }
    }

    /**
     * Handle the post "deleted" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function deleted(Post $post)
    {
        //
    }

    /**
     * Handle the comment "deleted" event.
     *
     * @param  \App\Models\Comment  $comment
     * @return void
     */
    public function deleting(Post $post)
    {
        $fileExists = Storage::disk('public')->exists(Post::GALLERY_PATH . $post->image);
        if($fileExists){
            Storage::disk('public')->delete(Post::GALLERY_PATH . $post->image);
        }
    }

    /**
     * Handle the post "restored" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function restored(Post $post)
    {
        //
    }

    /**
     * Handle the post "force deleted" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function forceDeleted(Post $post)
    {
        //
    }
}
