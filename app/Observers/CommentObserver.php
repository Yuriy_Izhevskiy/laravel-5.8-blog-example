<?php

namespace App\Observers;

use App\Models\Comment;
use Illuminate\Support\Facades\Storage;

class CommentObserver
{
    /**
     * Handle the comment "created" event.
     *
     * @param  \App\Models\Comment  $comment
     * @return void
     */
    public function created(Comment $comment)
    {
        //
    }

    /**
     * Handle the comment "updated" event.
     *
     * @param  \App\Models\Comment  $comment
     * @return void
     */
    public function updated(Comment $comment)
    {
        //
    }

    public function updating(Comment $comment)
    {
        /*** Delete old image before update image*/
        $oldNameImage = $comment->getOriginal('image');

        $fileExists = Storage::disk('public')->exists(Comment::GALLERY_PATH . $oldNameImage);

        if($comment->isDirty('image') && $fileExists){
            Storage::disk('public')->delete(Comment::GALLERY_PATH . $oldNameImage);
        }
    }

    /**
     * Handle the comment "deleted" event.
     *
     * @param  \App\Models\Comment  $comment
     * @return void
     */
    public function deleted(Comment $comment)
    {
        //
    }

    /**
     * Handle the comment "deleted" event.
     *
     * @param  \App\Models\Comment  $comment
     * @return void
     */
    public function deleting(Comment $comment)
    {
        $fileExists = Storage::disk('public')->exists(Comment::GALLERY_PATH . $comment->image);
        if($fileExists){
            Storage::disk('public')->delete(Comment::GALLERY_PATH . $comment->image);
        }
    }

    /**
     * Handle the comment "restored" event.
     *
     * @param  \App\Models\Comment  $comment
     * @return void
     */
    public function restored(Comment $comment)
    {
        //
    }

    /**
     * Handle the comment "force deleted" event.
     *
     * @param  \App\Models\Comment  $comment
     * @return void
     */
    public function forceDeleted(Comment $comment)
    {
        //
    }
}
