<?php

namespace App\Models;

use App\User;
use Cocur\Slugify\Slugify;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Traits\HasRoles;

class Post extends Model
{
    use HasRoles;

    const GALLERY_PATH = 'uploads/images/';
    const IMAGE_EMPTY_PATH = 'uploads/default/no-image.jpg';

    protected $fillable = [
        'title',
        'body',
        'description',
        'image',
        'alias',
        'category_id',
        'is_published'
    ];

    const selectArrayWithOutContent = [
        'id',
        'category_id',
        'title',
        'created_at',
        'updated_at',
        'description',
        'image',
        'is_published',
        'alias'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_published' => 'boolean',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function comment()
    {
        return $this->hasMany(Comment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getImagePathAttribute()
    {
        $noImage = Storage::disk('public')->exists(Post::IMAGE_EMPTY_PATH);

        if($this->image) {
            $this->alo;
        } else if ($noImage) {
            return self::IMAGE_EMPTY_PATH;
        }
    }

    public function getAloAttribute()
    {
        return self::GALLERY_PATH . $this->image;
    }

    public function getPublishedAttribute()
    {
        return ($this->is_published) ? 'success' : 'danger';
    }

    public function complete($id, $is_published = true)
    {
        $this->whereId($id)->update(compact('is_published'));
    }

    public function incomplete($id)
    {
        $this->complete($id, false);
    }

    public function scopeOnlyActive($query)
    {
        return $query->whereIsPublished(true);
    }
}
