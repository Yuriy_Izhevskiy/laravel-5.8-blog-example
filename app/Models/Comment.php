<?php

namespace App\Models;

use App\Models\Post;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Comment extends Model
{
    const GALLERY_PATH = 'uploads/images/comments/';
    const IMAGE_EMPTY_PATH = 'uploads/default/no-image.jpg';

    protected $fillable = [
        'body',
        'post_id',
        'is_published',
        'post_id',
        'user_name',
        'user_email',
        'image'
    ];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getImagePathAttribute()
    {
        $noImage = Storage::disk('public')->exists(self::IMAGE_EMPTY_PATH);

        if($this->image) {
            return self::GALLERY_PATH . $this->image;
        } else if ($noImage) {
            return self::IMAGE_EMPTY_PATH;
        }
    }

    public function getPublishedAttribute()
    {
        return ($this->is_published) ? 'success' : 'danger';
    }


}
