<?php

namespace App\Policies;

use App\User;
use Spatie\Permission\Models\Permission;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    public function delete(User $user, Permission $permission)
    {
        if ($permission->name == "Administer roles & permissions") {
            return false;
        }
        return true;
    }

}
