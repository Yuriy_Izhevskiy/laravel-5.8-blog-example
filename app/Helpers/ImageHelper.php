<?php

namespace App\Helpers;


use App;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;


class ImageHelper
{
    const SEPARATOR = '-';

    static public function generateName($path, UploadedFile $file, $disk = 'public')
    {
        $extension = $file->getClientOriginalExtension();

        $name = explode(self::SEPARATOR, $file->getClientOriginalName());
        $filename = self::checkName($name[0]);

        return "$filename.$extension";

//        $name = $file->getClientOriginalName();
//
//        unset($name[count($name) - 1]);
//        $name = implode(self::SEPARATOR, $name);
//        $name = self::transformName($name);
//        $name = self::checkName($name);
//
//        return $name . '.' . $ext;
    }

    static public function checkName($name)
    {
        return $name . self::SEPARATOR . time();
    }

//    static public function transformName($title, $language = 'en')
//    {
//        $title = $language ? Str::ascii($title, $language) : $title;
//
//        // Convert all dashes/underscores into separator
//        $flip = self::SEPARATOR === '-' ? '_' : '-';
//
//        $title = preg_replace('![' . preg_quote($flip) . ']+!u', self::SEPARATOR, $title);
//
//        // Replace @ with the word 'at'
//        $title = str_replace('@', self::SEPARATOR . 'at' . self::SEPARATOR, $title);
//
//        // Remove all characters that are not the separator, letters, numbers, or whitespace.
//        $title = preg_replace('![^' . preg_quote(self::SEPARATOR) . '\pL\pN\s]+!u', '', mb_strtolower($title));
//
//        // Replace all separator characters and whitespace by a single separator
//        $title = preg_replace('![' . preg_quote(self::SEPARATOR) . '\s]+!u', self::SEPARATOR, $title);
//
//        //remove - from end alias
//        $title = preg_replace('!(.+)' . preg_quote('-') . '$!u', '', $title);
//
//        //remove - from start alias
//        $title = preg_replace('!^' . preg_quote('-') . '(.+)!u', '', $title);
//
//        return trim($title, self::SEPARATOR);
//    }


}