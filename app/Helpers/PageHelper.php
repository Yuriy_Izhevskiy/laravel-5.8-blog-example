<?php
namespace App\Helpers;

use App;
use Illuminate\Support\Facades\Request;

class PageHelper
{
    static public function setActive($path)
    {
        return Request::is($path . '*') ? 'active' :  '';
    }
}